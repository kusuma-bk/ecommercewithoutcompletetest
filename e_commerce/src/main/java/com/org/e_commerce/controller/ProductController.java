package com.org.e_commerce.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.org.e_commerce.dto.ProductDto;
import com.org.e_commerce.dto.ProductResponseDto;
import com.org.e_commerce.dto.ProductsListResponseDto;
import com.org.e_commerce.exception.ProductDetailsException;
import com.org.e_commerce.exception.ProductException;
import com.org.e_commerce.service.ProductService;

@RestController
@RequestMapping("/products")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ProductController {

	private static final Logger logger = LoggerFactory.getLogger(ProductController.class);
	@Autowired
	ProductService productService;

	@GetMapping("/productName")
	public ResponseEntity<ProductsListResponseDto> searchProduct(@RequestParam String productName)
			throws ProductException {
		logger.info("Inside ProductController of searchProduct method ");
		return new ResponseEntity<>(productService.searchProduct(productName), HttpStatus.OK);
	}
	@PostMapping
	public ResponseEntity<ProductResponseDto> addProducts(@RequestBody ProductDto productDto)
			throws ProductDetailsException {
		ProductResponseDto response = productService.addProducts(productDto);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

}
