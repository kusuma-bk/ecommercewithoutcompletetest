package com.org.e_commerce.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CustomerDto {
	private String customerName;
	private String phoneNumber;
	private String emailId;
	private String password;
	private String customerType;

}
