package com.org.e_commerce.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter

public class PurchaseDto {
	private long productId;
	private long customerId;
	private int quantity;
	private int transactionId;

}
