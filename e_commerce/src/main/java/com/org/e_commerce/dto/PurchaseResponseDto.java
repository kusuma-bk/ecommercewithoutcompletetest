package com.org.e_commerce.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter

public class PurchaseResponseDto {
	private String message;
	private int status;
}
