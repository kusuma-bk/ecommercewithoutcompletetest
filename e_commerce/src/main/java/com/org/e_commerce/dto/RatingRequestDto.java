package com.org.e_commerce.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RatingRequestDto {
	private int customerRating;
    private long purchaseId;

}
