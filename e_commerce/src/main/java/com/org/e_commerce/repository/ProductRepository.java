package com.org.e_commerce.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.org.e_commerce.dto.ProductListResponseDto;
import com.org.e_commerce.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
	List<Product> findByProductNameIgnoreCaseContaining(String productName);

	Optional<Product> findByProductId(long productId);

	Product findByProductNameIgnoreCase(String productName);

	@Query("select p.productName,p.productPrice,p.description,pd.productRating from Product p left join ProductDetails pd on p.productId= pd.productDetailsId order by pd.productRating desc")
	List<?> findByProductDetailsByPriority();

	@Query("select p.productName,p.productPrice,p.description,pd.productRating from Product p left join ProductDetails pd on p.productId=pd.productDetailsId order by pd.productRating desc")
	List<?> findByProductDetailsByNormal(Pageable pageable);

}
