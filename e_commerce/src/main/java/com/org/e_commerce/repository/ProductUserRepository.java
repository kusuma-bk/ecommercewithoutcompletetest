package com.org.e_commerce.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.org.e_commerce.entity.ProductUser;

@Repository
public interface ProductUserRepository extends JpaRepository<ProductUser, Long> {
	List<ProductUser> findByCustomerType(String customerType);
}
