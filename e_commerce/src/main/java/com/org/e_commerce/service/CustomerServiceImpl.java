package com.org.e_commerce.service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.org.e_commerce.dto.CustomerDto;
import com.org.e_commerce.dto.CustomerLoginResponseDto;
import com.org.e_commerce.dto.LoginRequestDto;
import com.org.e_commerce.dto.ProductListResponseDto;
import com.org.e_commerce.entity.Customer;
import com.org.e_commerce.entity.Product;
import com.org.e_commerce.entity.ProductDetails;
import com.org.e_commerce.exception.UserAlreadyExists;
import com.org.e_commerce.exception.UserExistsErrorException;
import com.org.e_commerce.exception.UserPasswordErrorException;
import com.org.e_commerce.repository.CustomerRepository;
import com.org.e_commerce.repository.ProductDetailsRepository;
import com.org.e_commerce.repository.ProductUserRepository;
import com.org.e_commerce.repository.ProductRepository;
import com.org.e_commerce.utility.CustomerUtility;

@Service
public class CustomerServiceImpl implements CustomerService {

	private static final Logger logger = LoggerFactory.getLogger(CustomerServiceImpl.class);

	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	ProductRepository productsRepository;

	@Autowired
	ProductUserRepository productUserRepository;

	@Autowired
	ProductDetailsRepository productDetailsRepository;

	@Autowired
	ProductRepository productRepository;

	public String customerRegistartion(CustomerDto customerDto) throws UserAlreadyExists {
		Customer customer;
		logger.info("inside customerRegistartion method of CustomerServiceImpl class");

		Optional<Customer> userDetail = customerRepository.findByEmailId(customerDto.getEmailId());

		if (userDetail.isPresent()) {
			throw new UserAlreadyExists(CustomerUtility.USERALREADY_EXISTS);
		}
		customer = new Customer();
		BeanUtils.copyProperties(customerDto, customer);
		customerRepository.save(customer);
		return "CustomerDetails registered Successfully";
	}

	@Override
	public CustomerLoginResponseDto customerLogin(LoginRequestDto loginRequestDto)
			throws UserExistsErrorException, UserPasswordErrorException {
		logger.info("inside customerLogin method of CustomerServiceImpl class");
		Optional<Customer> userDetail = customerRepository.findByEmailIdAndPassword(loginRequestDto.getEmailId(),
				loginRequestDto.getPassword());
		if (!userDetail.isPresent()) {

			Optional<Customer> userDetail1 = customerRepository.findByEmailId(loginRequestDto.getEmailId());
			if (!userDetail1.isPresent())

				throw new UserExistsErrorException(CustomerUtility.USER_ERROR);

			throw new UserPasswordErrorException(CustomerUtility.USER_PSWD_ERROR);
		}

		CustomerLoginResponseDto customerLoginResponseDto = new CustomerLoginResponseDto();
		BeanUtils.copyProperties(userDetail.get(), customerLoginResponseDto);
		customerLoginResponseDto.setStatuscode(611);
		Random rand = new Random();

		customerLoginResponseDto.setLoginId(rand.nextInt(10000));
		return customerLoginResponseDto;
	}

	public List<ProductListResponseDto> productList(long customerId) throws UserExistsErrorException {
		Customer customer = customerRepository.findByCustomerId(customerId);
		if (ObjectUtils.isEmpty(customer)) {
			throw new UserExistsErrorException(CustomerUtility.CUSTOMER_EXISTS_ERROR);
		} else {
			String userType = customer.getCustomerType();
			if ((userType.equalsIgnoreCase("priority")) || (userType.equalsIgnoreCase("admin"))) {
				List<ProductListResponseDto> arrayList = new ArrayList<>();
				List<Product> products = productRepository.findAll();
				for (Product object : products) {
					ProductListResponseDto productListResponseDtoIn = new ProductListResponseDto();
					BeanUtils.copyProperties(object, productListResponseDtoIn);
					Optional<ProductDetails> productDetails = productDetailsRepository
							.findByProductId(object.getProductId());
					if (productDetails.isPresent() && productDetails.get().getQuantity() > 0) {
						float d = productDetails.get().getProductRating();
						DecimalFormat f = new DecimalFormat("#.0");

						float ratingValue = Float.parseFloat(f.format(d));
						productListResponseDtoIn.setProductRating(ratingValue);
						productListResponseDtoIn.setQuantity(productDetails.get().getQuantity());
						arrayList.add(productListResponseDtoIn);
					}

				}
				List<ProductListResponseDto> sortedUsers = arrayList.stream()
						.sorted(Comparator.comparing(ProductListResponseDto::getProductRating).reversed())
						.collect(Collectors.toList());

				return sortedUsers;

			} else {
				Long count = productRepository.count();

				PageRequest pagable = (PageRequest.of(0, (int) (count / 2),
						Sort.by(Sort.Direction.DESC, "productPrice")));
				ArrayList<ProductListResponseDto> arrayList = new ArrayList<>();
				Page<Product> products = productRepository.findAll(pagable);
				Optional<ProductDetails> productDetails;
				for (Product object : products) {
					ProductListResponseDto productListResponseDtoIn = new ProductListResponseDto();
					BeanUtils.copyProperties(object, productListResponseDtoIn);
					productDetails = productDetailsRepository.findByProductId(object.getProductId());

					if (productDetails.isPresent() && productDetails.get().getQuantity() > 0) {

						float d = productDetails.get().getProductRating();
						DecimalFormat f = new DecimalFormat("#.0");

						float ratingValue = Float.parseFloat(f.format(d));
						System.out.println("rating value = " + ratingValue);

						productListResponseDtoIn.setProductRating(ratingValue);
						productListResponseDtoIn.setQuantity(productDetails.get().getQuantity());
						arrayList.add(productListResponseDtoIn);
					}

				}
				List<ProductListResponseDto> sortedUsers = arrayList.stream()
						.sorted(Comparator.comparing(ProductListResponseDto::getProductRating).reversed())
						.collect(Collectors.toList());

				return sortedUsers;
			}
		}

	}
}
