package com.org.e_commerce.service;

import com.org.e_commerce.dto.ProductDto;
import com.org.e_commerce.dto.ProductResponseDto;
import com.org.e_commerce.dto.ProductsListResponseDto;
import com.org.e_commerce.exception.ProductDetailsException;
import com.org.e_commerce.exception.ProductException;

public interface ProductService {

	ProductsListResponseDto searchProduct(String productName) throws ProductException;

	public ProductResponseDto addProducts(ProductDto productDto) throws ProductDetailsException;
}
