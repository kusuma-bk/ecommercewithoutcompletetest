package com.org.e_commerce.utility;

public class CustomerUtility {
	
	private CustomerUtility() {
	    throw new IllegalStateException("CustomerUtility class");
	  }
	
	
	public static final String CUSTOMER_EXISTS_ERROR = "Customer does not exists!";
	public static final int CUSTOMER_EXISTS_ERROR_STATUS = 605;

	public static final String TRY_AGAIN = "Unable to save the details something went wrong!!";
	public static final int TRY_AGAIN_STATUSCODE = 600;
	
	public static final String USER_ERROR = "EmailId is not exsist Please Register";
    public static final int USER_ERROR_STATUS = 601;
   
    public static final String USER_PSWD_ERROR = "password is not correct";
    public static final int USER_PSWD_ERROR_STATUS = 602;
    
    public static final String USERALREADY_EXISTS = "Customer  already Exists";
    public static final int USERALREADY_EXISTS_STATUSCODE = 603;
    
    
    
   
}
