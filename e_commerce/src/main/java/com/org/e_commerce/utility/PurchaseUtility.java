package com.org.e_commerce.utility;

public class PurchaseUtility {
	private PurchaseUtility() {
		throw new IllegalStateException("PurchaseUtility class");
	}

	public static final String QUANTITY_ERROR = "Sorry Quantity of Products You Want is not Available";
	public static int QUANTITY_ERROR_STATUSCODE = 620;

	public static final String QUANTITY_UNAVAILABLE = "Sorry, quantity is not available";
	public static int QUANTITY_UNAVAILABLE_STATUSCODE = 622;
}
