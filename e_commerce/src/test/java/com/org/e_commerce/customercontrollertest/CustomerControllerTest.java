package com.org.e_commerce.customercontrollertest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.org.e_commerce.controller.CustomerController;
import com.org.e_commerce.controller.LoginController;
import com.org.e_commerce.controller.PurchaseController;
import com.org.e_commerce.dto.CustomerDto;
import com.org.e_commerce.dto.CustomerLoginResponseDto;
import com.org.e_commerce.dto.LoginRequestDto;
import com.org.e_commerce.dto.ProductListResponseDto;
import com.org.e_commerce.dto.PurchaseDto;
import com.org.e_commerce.dto.PurchaseResponseDto;
import com.org.e_commerce.exception.CustomerException;
import com.org.e_commerce.exception.QuantityException;
import com.org.e_commerce.exception.QuantityUnavailable;
import com.org.e_commerce.exception.UserAlreadyExists;
import com.org.e_commerce.exception.UserExistsErrorException;
import com.org.e_commerce.exception.UserPasswordErrorException;
import com.org.e_commerce.service.CustomerService;
import com.org.e_commerce.service.PurchaseService;

@RunWith(MockitoJUnitRunner.class)
public class CustomerControllerTest {

	@Mock
	CustomerService customerService;

	@InjectMocks
	CustomerController customerController;

	@InjectMocks
	LoginController loginController;

	@Mock
	PurchaseService purchaseService;

	@InjectMocks
	PurchaseController purchaseController;

	CustomerLoginResponseDto customerLoginResponseDto;

	LoginRequestDto loginRequestDto;

	List<ProductListResponseDto> listOfProduct;

	long customerId;

	CustomerDto customerDto;

	PurchaseDto purchaseDto;
	PurchaseResponseDto purchaseResponseDto;
	List<ProductListResponseDto> productListResponseDto;

	@Before
	public void setup() {

		customerId = 1l;
		listOfProduct = new ArrayList<ProductListResponseDto>();

		customerDto = new CustomerDto();
		customerDto.setCustomerName("kusuma");
		customerDto.setCustomerType("priority");
		customerDto.setEmailId("kusuma@gmail.com");
		customerDto.setPassword("456");
		customerDto.setPhoneNumber("8765788590");

		customerLoginResponseDto = new CustomerLoginResponseDto();
		customerLoginResponseDto.setCustomerId(1);
		customerLoginResponseDto.setCustomerName("suppi");
		customerLoginResponseDto.setEmailId("suppi@gmail.com");

		loginRequestDto = new LoginRequestDto();
		loginRequestDto.setEmailId("suppi@hcl.com");
		loginRequestDto.setPassword("123");

		purchaseDto = new PurchaseDto();
		purchaseDto.setCustomerId(2);
		purchaseDto.setProductId(1);
		purchaseDto.setQuantity(2);

		purchaseResponseDto = new PurchaseResponseDto();
		purchaseResponseDto.setMessage("Successfully you purchase the Product");
		purchaseResponseDto.setStatus(621);

		productListResponseDto = new ArrayList<>();

	}

	@Test
	public void customerRegistration() throws CustomerException, UserAlreadyExists {
		String message = "CustomerDetails Added Successfully";
		Mockito.when(customerService.customerRegistartion(Mockito.any(CustomerDto.class))).thenReturn(message);

		ResponseEntity<String> response = customerController.customerRegistartion(customerDto);
		Assert.assertEquals("CustomerDetails Added Successfully", response.getBody());

	}


	@Test
	public void customerLoginTest() throws UserExistsErrorException, UserPasswordErrorException {
		Mockito.when(customerService.customerLogin(Mockito.any(LoginRequestDto.class)))
				.thenReturn(customerLoginResponseDto);

		ResponseEntity<CustomerLoginResponseDto> response = loginController.checkLoginByUserId(loginRequestDto);

		Assert.assertNotNull(response);

		Assert.assertEquals(customerLoginResponseDto, response.getBody());
	}

	@Test
	public void buyProductTest() throws QuantityException, QuantityUnavailable {

		Mockito.when(purchaseService.buyProduct(purchaseDto)).thenReturn(purchaseResponseDto);

		ResponseEntity<PurchaseResponseDto> response = purchaseController.buyProduct(purchaseDto);
		Assert.assertEquals(purchaseResponseDto, response.getBody());

	}

	/*
	 * @Test public void productsList() throws UserExistsErrorException {
	 * Mockito.when(customerService.productList(Mockito.anyLong())).thenReturn(
	 * productListResponseDto);
	 * 
	 * ResponseEntity<List<ProductListResponseDto>> response =
	 * customerController.productsList(customerId); Assert.assertNotNull(response);
	 * Assert.assertEquals(productListResponseDto, response.getBody());
	 * 
	 * }
	 */

}
