package com.org.e_commerce.customercontrollertest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.org.e_commerce.controller.PurchaseController;
import com.org.e_commerce.dto.PurchaseListResponseDto;
import com.org.e_commerce.dto.RatingRequestDto;
import com.org.e_commerce.dto.RatingResponseDto;
import com.org.e_commerce.service.PurchaseService;

@RunWith(MockitoJUnitRunner.class)
public class PurchaseControllerTest {
	@Mock
	PurchaseService purchaseService;

	@InjectMocks
	PurchaseController purchaseController;

	RatingRequestDto ratingRequestDto;
	RatingResponseDto ratingResponseDto;
	List<PurchaseListResponseDto> purchaseListResponseDto;
	PurchaseListResponseDto purchaseResponseDto;
	long customerId;

	@Before
	public void setup() {
		purchaseListResponseDto = new ArrayList<>();
		purchaseResponseDto = new PurchaseListResponseDto();
		ratingRequestDto = new RatingRequestDto();
		customerId = 1;
		purchaseResponseDto.setPurchaseId(1);
		purchaseResponseDto.setQuantity(7);
		purchaseResponseDto.setDate("2011-10-10");
		purchaseResponseDto.setDescription("mobile-36MP");
		purchaseResponseDto.setProductPrice(30000);
		purchaseResponseDto.setProductName("Redmi");
		purchaseResponseDto.setTime("10pm");
	
		purchaseListResponseDto.add(purchaseResponseDto);

		ratingRequestDto.setCustomerRating(5);
		ratingRequestDto.setPurchaseId(1);
		
		ratingResponseDto = new RatingResponseDto();
		ratingResponseDto.setMessage("thanks for your feedback");
		ratingResponseDto.setStatus(611);
	}

	@Test
	public void ratingTest() {
		Mockito.when(purchaseService.rating(Mockito.any(RatingRequestDto.class))).thenReturn(ratingResponseDto);

		ResponseEntity<RatingResponseDto> response = purchaseController.rating(ratingRequestDto);

		Assert.assertNotNull(response);

		Assert.assertEquals(ratingResponseDto, response.getBody());
	}

	@Test
	public void purchaseList() {
		Mockito.when(purchaseService.purchaseList(Mockito.anyLong())).thenReturn(purchaseListResponseDto);

		ResponseEntity<List<PurchaseListResponseDto>> response = purchaseController.purchaseList(customerId);

		Assert.assertNotNull(response);
		Assert.assertEquals(purchaseListResponseDto, response.getBody());
	}
	
	

}
